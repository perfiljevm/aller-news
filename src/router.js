import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import NewsPage from "./pages/NewsPage";
import RestorePopup from "./components/RestorePopup";
import Header from "./components/Header";


export default function AppRouter() {
  

  return (
    <Router>
      <Header/>
      <div className="container">
        <Route exact path="/" component={NewsPage} />
        <Route path="/column" component={NewsPage} />
      </div>
      <RestorePopup />
    </Router>
  );
}
