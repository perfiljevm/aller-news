import {
  DELETE_ARTICLE,
  GET_ARTICLES,
  UPDATE_ARTICLE,
  RESTORE_ARTICLE,
  HIDE_POPUP,
  SAVE_UPDATE,
} from "../actionTypes";

import update from "react-addons-update";

const defaultState = {
  showPopup: false,
  data: [],
  deleted: {
    index: 0,
    article: {},
  },
};

const articleList = (state = defaultState, action) => {
  switch (action.type) {
    case DELETE_ARTICLE: {
      return {
        ...state,
        deleted: {
          index: action.payload,
          article: state.data[action.payload],
        },
        showPopup: true,
        data: state.data.filter((article, index) => index !== action.payload),
      };
    }
    case SAVE_UPDATE: {
      const { id, text } = action.payload;
      return update(state, {
        data: {
          [id]: {
            title: { $set: text },
            inputHeader: {$set: false}
          },
        },
      });
    }
    case UPDATE_ARTICLE: {
      const { id } = action.payload;
      state.data.forEach((item) => {
        item.inputHeader = false
      })
      return update(state, {
        data: {
          [id]: {
            inputHeader: {$set: true}
          },
        },
      });
    }
    case RESTORE_ARTICLE: {
      const articlesArr = [...state.data];
      if(state.deleted) {
        articlesArr.splice(state.deleted.index, 0, state.deleted.article);
      }
      return {
        ...state,
        showPopup: false,
        data: articlesArr,
        deleted: null
      };
    }
    case GET_ARTICLES: {
      return {
        ...state,
        data: action.payload,
      };
    }
    case HIDE_POPUP: {
      return {
        ...state,
        showPopup: false,
      };
    }
    default:
      return state;
  }
};

export default articleList;
