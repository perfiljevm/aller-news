export const getArticleList = store => store.articleList;

export const getArticles = store => getArticleList(store);

