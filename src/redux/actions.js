import axios from "axios";
import {
  GET_ARTICLES,
  UPDATE_ARTICLE,
  DELETE_ARTICLE,
  RESTORE_ARTICLE,
  HIDE_POPUP,
  SAVE_UPDATE,
} from "./actionTypes";

export const getArticle = (res) => ({
  type: GET_ARTICLES,
  payload: res,
});

export const updateArticle = (id) => ({
  type: UPDATE_ARTICLE,
  payload: {
    id,
  },
});

export const saveArticle = (id, text) => ({
  type: SAVE_UPDATE,
  payload: {
     id,text 
  },
})

export const deleteArticle = (id) => ({
  type: DELETE_ARTICLE,
  payload: id,
});

export const restoreArticle = () => ({
  type: RESTORE_ARTICLE,
});

export const hidePopup = () => ({
  type: HIDE_POPUP,
});

export const getArticles = () => {
  return async (dispatch) => {
    await axios
      .get("https://storage.googleapis.com/aller-structure-task/test_data.json")
      .then( (res) =>  {
        /*
        According to recieved json. I have admit that it quite uncomfortable.
        That's why i have decided to reconstuct it on my own format
        */
        const articleColumns = res.data.flat().map((item, index) => item.columns);

        const articles = articleColumns.flat().map((item, index) => {
          return {...item, inputHeader: false}
        });
        /*Add assign field in item for solve issue with unclosed input fields*/
        dispatch(getArticle(articles));
      });
  };
};
