export const GET_ARTICLES = "GET_ARTICLES";
export const UPDATE_ARTICLE = "UPDATE_ARTICLE";
export const SAVE_UPDATE = "SAVE_UPDATE";
export const DELETE_ARTICLE = "DELETE_ARTICLE";
export const RESTORE_ARTICLE = "RESTORE_ARTICLE";
export const HIDE_POPUP = 'HIDE_POPUP';