import React, {useEffect} from "react";
import {  useDispatch } from "react-redux";
import AppRouter from './router.js';

import {getArticles} from "./redux/actions";

import './App.css';

function App() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getArticles())
  });

  return (
    <div className="App">
        <AppRouter />
    </div>
  );
}

export default App;
