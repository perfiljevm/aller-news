import React from 'react';
import { useSelector} from "react-redux";
import { getArticles } from "../redux/selectors";

import Article from '../components/Article';

import Grid from '@material-ui/core/Grid';



function NewsPage (){
    const {data} = useSelector(getArticles);
    return(
        <Grid container className={"grid-container"}>
          {
            data && data.map((article, index) => {
              return(
                <Article data={article} key={index} index={index}/>
              )             
            })
          }
        </Grid>
    )
}

export default NewsPage;