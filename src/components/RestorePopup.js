import React from 'react';

import { useSelector, useDispatch } from "react-redux";
import { getArticles } from "../redux/selectors";

import {
  restoreArticle,
  hidePopup,
} from "../redux/actions";

import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";


export default function RestorePopup() {
  const dispatch = useDispatch();
  const { showPopup } = useSelector(getArticles);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    dispatch(hidePopup());
  };

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={showPopup}
        autoHideDuration={6000}
        onClose={handleClose}
        message="Note archived"
        action={
          <React.Fragment>
            <Button
              color="secondary"
              size="small"
              onClick={() => dispatch(restoreArticle())}
            >
              UNDO
            </Button>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={() => dispatch(hidePopup())}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </div>
  );
}
