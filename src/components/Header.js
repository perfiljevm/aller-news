import React, {useState} from "react";

import { makeStyles } from "@material-ui/core/styles";
import {AppBar, Toolbar, Typography, IconButton} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
  }));

export default function Header () {
  const classes = useStyles();
  const history = useHistory();
  const [path, setPath] = useState('/')

  let pageType = path === "/column" ? "/" : "/column";

  function handlePath() {
    setPath(pageType)
    history.push(pageType)
  }
    return (
    <AppBar position="sticky">
        <Toolbar variant="dense">
            <IconButton
              edge="start"
              className={classes.menuButton}
              aria-label="menu"
              onClick={handlePath}
            >
              <MenuIcon className="icons" />
            </IconButton>
          <Typography variant="h1" color="inherit">
            Aller News
          </Typography>
        </Toolbar>
      </AppBar>
      )
}