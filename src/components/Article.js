import React, { useRef } from "react";

import {
  Card,
  CardHeader,
  CardMedia,
  TextField ,
  Grid,
  IconButton
} from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";
import CreateIcon from "@material-ui/icons/Create";
import CloseIcon from "@material-ui/icons/Close";
import DoneIcon from "@material-ui/icons/Done";
import { deleteArticle, updateArticle, saveArticle } from "../redux/actions";
import { useDispatch } from "react-redux";
import { useLocation } from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "400px",
  },
  container: {
    display: "grid",
  },
  Card: {
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100%",
    width: "100%",
    marginTop: "10px",
    background: "#3f51b5",
  },
  textField: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',            
    paddingBottom: 0,
    marginTop: 0,
    fontWeight: 500
},
  input: {
    color: 'white'
  },
  media: {    
    position: "relative",
    paddingBottom: "56.2%",
  },
}));

export default function Article(data) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { inputHeader, width, title, imageUrl } = data.data;
  const textInput = useRef();
  let location = useLocation();
  let column = location.pathname === "/column" ? 12 : width;
  
  function HeaderButton() {
    if (inputHeader) {
      return (
        <IconButton
          onClick={() => {
            console.log(textInput)
            dispatch(saveArticle(data.index, textInput.current.value));
          }}
          aria-label="settings"
        >
          <DoneIcon className="icons" />
        </IconButton>
      );
    } else {
      return (
        <IconButton
          onClick={() => {
            dispatch(updateArticle(data.index));
          }}
          aria-label="settings"
        >
          <CreateIcon className="icons" />
        </IconButton>
      );
    }
  }

  function CardInput() {
    if (inputHeader) {
      return (
        <TextField
          type="text"
          maxLength="64"
          inputRef={textInput}
          key={data.index}
          defaultValue={title}
          className={classes.textField}
          InputProps={{
            className: classes.input,
        }}
        />
      );
    } else {
      return <div className="title">{title}</div>;
    }
  }
  return (
    <Grid className="article-container" item xs={column}>

    <Card className={classes.Card} key={data.index}>
      <CardMedia
        className={classes.media}
        image={`${imageUrl}&width=710&height=500`}
        title="Paella dish"
      />
      <CardHeader
        action={
          <div className={"icon-buttons"}>
            <HeaderButton />
            <IconButton
              onClick={() => {
                dispatch(deleteArticle(data.index));
              }}
              aria-label="delete article"
            >
              <CloseIcon className="icons" />
            </IconButton>
          </div>
        }
        title={<CardInput />}
      />
    </Card>
    </Grid>
  );
}
